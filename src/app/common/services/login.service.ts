import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Utilisateur } from '../data/Utilisateur';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { Token } from '../data/Token';
interface Request {

  username: String;
  pwd: String;
}
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private readonly JWT_TOKEN: string = 'JWT_TOKEN';
  private readonly USER_ID:string = 'USER_ID';
  private readonly USER_NAME:string = 'USER_NAME';


  user:Utilisateur=new Utilisateur();
  constructor(private http: HttpClient, private router: Router) { }
  private _headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  
  baseUrlNode="http://localhost:3000";

  authenticate(request: Request): Observable<any> {
       let baseUrl: string = this.baseUrlNode+"/login";
    return this.http.post<any>(baseUrl, request, { headers: this._headers })
    .pipe(
      tap(userInfo => {
        console.log("userlogin info "+userInfo.id)
        this.storeUserId(userInfo.id),
        this.storeTokens(userInfo.token)}),
      mapTo(true),
      catchError(error => {
        if(error.status==400)
        alert("Vous devez d'abord vous inscrire");
        return of(false);
      }));
     }


  

  ajouterUnUser(user: Request): Observable<any> {

       let baseUrl: string = this.baseUrlNode+"/register";

    return this.http.post<any>(baseUrl, user, { headers: this._headers })
    .pipe(
      tap((data) => {this.storeUserId(data.id),this.storeTokens(data.token)}),
      catchError(error => {
      
        return of(false);
  }))
}


  private storeTokens(tokens: Token) {
    localStorage.setItem(this.JWT_TOKEN, JSON.stringify(tokens));
 
  }


  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  getUserId() :string{
    return localStorage.getItem(this.USER_ID);
  }

  getUsername() :string{
    return localStorage.getItem(this.USER_NAME);
  }


  public storeUserId(id:Number){
    localStorage.setItem(this.USER_ID,JSON.stringify(id));
  }

  public storeUsername(name:String){
    localStorage.setItem(this.USER_NAME,JSON.stringify(name));
  }

 

}
