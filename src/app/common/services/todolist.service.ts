import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { List } from '../data/List';
import { Tache } from '../data/Tache';

@Injectable({
  providedIn: 'root'
})
export class TodolistService {

  constructor(private http: HttpClient, private router: Router) { }
  private _headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  baseUrlNode = "http://localhost:4545";

  afficherMylist(userid: String): Observable<any> {

    return this.http.get<any>(this.baseUrlNode + `/mylist/${userid}/lists`,
      { responseType: "text" as "json" });
  }


  afficherToutesLesTaches(titre: String): Observable<any> {

    return this.http.get<any>(this.baseUrlNode + `/mylist/${titre}`
    );

  }

  ajouterUneTache(titre: String, tache: Tache): Observable<any> {
    let baseUrl = this.baseUrlNode + `/mylist/${titre}`
    return this.http.put<any>(baseUrl, tache, { headers: this._headers })
  }

  ajouterUneList(userid:String,list:List): Observable<any> {
    let baseUrl = this.baseUrlNode + `/mylist/${userid}`
    return this.http.post<any>(baseUrl, list, { headers: this._headers })
  }
}
