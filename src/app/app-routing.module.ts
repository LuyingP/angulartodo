import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { TacheComponent } from './tache/tache.component';

const routes: Routes = [

  { path: "login", component: LoginComponent },
  { path: "registrer", component: LoginComponent },
  { path: "mylist/:userid", component: HomeComponent },
  { path: "**", redirectTo: "/login" }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
