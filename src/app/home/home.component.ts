import { Component, OnInit } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';
import{ List } from '../common/data/List'
import{ Tache } from '../common/data/Tache'
import { LoginService } from '../common/services/login.service';
import { TodolistService } from '../common/services/todolist.service';

import { TacheComponent } from '../tache/tache.component';

import {MatDialog} from '@angular/material/dialog'
import { ListComponent } from '../list/list.component';

@Component({
  //   nb-sidebar-toggle, nb-layout-subheader
  selector:  "nb-layout-subheader",
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  hidden:Boolean
  hiddenList:Boolean
  userid: String
  list:List=new List()
  listsToDo:List[]=[
  ];
  onetache:Tache=new Tache();
  tacheList:Tache[]
  titre_list_choisi:String

  constructor(private loginService: LoginService,private todolistService: TodolistService,
    private sidebarService: NbSidebarService,private dialog:MatDialog

    ) { }

  ngOnInit(): void {

    this.userid=this.loginService.getUserId().slice(1, -1)
    this.todolistService.afficherMylist(this.userid)
    .subscribe(data=> {this.listsToDo=JSON.parse(data),
      console.log("list? "+(this.listsToDo instanceof Array)
      // +" user's listsToDo"+JSON.stringify(this.listsToDo)
      )})
  
    this.hidden=false;
  }

  toggle() {
     this.sidebarService.toggle(true);
     return false;
  }


  onDisplay(){
    this.hidden=true;
  }

  onHide(){
    this.hidden=false;
  }

  onDisplayList(){
    this.hiddenList=true;
  }

  onHideList(){
    this.hiddenList=false;
  }

  onDetail(titre_list:String){
   this.titre_list_choisi=titre_list
    this.todolistService.afficherToutesLesTaches(titre_list)
    .subscribe(data=>{this.list=data[0]
      // ,console.log("******"+this.list.taches[0].titre_tache+"    "+(this.list.taches instanceof Array))
    })


  }

  onDetailTache(index:number,listSelected:List){
    console.log("index selected "+index)
    console.log("tache détaillée "+JSON.stringify(this.list.taches[index]))
    console.log("list selected "+JSON.stringify(listSelected))
    console.log("affiché "+listSelected.taches[index].titre_tache)

    this.onetache.titre_tache=listSelected.taches[index].titre_tache.toUpperCase()
    this.onetache.description=listSelected.taches[index].description
    this.onetache.date_fin=listSelected.taches[index].date_fin
    this.onetache.date_creation=listSelected.taches[index].date_creation_string
    console.log("date affiché "+this.onetache.date_creation)
    

  }

  openWindow() {
    console.log("titre chosir "+this.titre_list_choisi)
     let dialogRef=this.dialog.open(TacheComponent);
    dialogRef.afterClosed()
    .subscribe(result=>
      {console.log(result instanceof Tache);
      this.onetache=result;
     this.todolistService.ajouterUneTache(this.titre_list_choisi,result)
     .subscribe(data=>{console.log("new tache list "+JSON.stringify(data));
     this.onDetail(this.titre_list_choisi)
      })
    }
      )
  }


  ajouterUneList(){
    let id=this.loginService.getUserId().slice(1, -1)
    let dialogRef=this.dialog.open(ListComponent);
    dialogRef.afterClosed()
   .subscribe(result=>
     {console.log(result instanceof List);
     this.list=result;
    this.todolistService.ajouterUneList(id,this.list)
    .subscribe(data=>{console.log("new list"+JSON.stringify(data));
    this.ngOnInit()
     })
   }
     )

  }


 
}

