import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NbThemeModule, NbLayoutModule, NbSidebarService, NbMenuService, NbRadioModule, NbWindowModule, NbDialogService } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NbSidebarModule, NbButtonModule , NbActionsModule} from '@nebular/theme';
import { TacheComponent } from './tache/tache.component';
import { MatDialogModule} from '@angular/material/dialog';
import { ListComponent } from './list/list.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    TacheComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbEvaIconsModule,
    NbLayoutModule,
    NbButtonModule,
    RouterModule,
    NbActionsModule,
    NbSidebarModule,
    NbRadioModule,
    MatDialogModule
   
   

  ],
  providers: [NbSidebarService, NbMenuService,NbDialogService],
  bootstrap: [AppComponent],
  entryComponents:[TacheComponent]
})
export class AppModule { }
