import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CustomvalidationService } from '../common/services/customvalidation.service';
import { Utilisateur} from '../common/data/Utilisateur'
import { Request} from '../common/data/Request'
import { LoginService} from '../common/services/login.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  
  user:Utilisateur=new Utilisateur();
  request:Request=new Request();
  registreForm: FormGroup;
  submitted = false;
  authType: String = "";
  errMsg:String;


  constructor(public loginService:LoginService,private fb: FormBuilder,
    private customValidator: CustomvalidationService,private route: ActivatedRoute, private rout: Router,) { 
      this.registreForm = this.fb.group({
         username: ['', [Validators.required]],
        pwd: ['', Validators.compose([Validators.required, this.customValidator.patternValidator()])],
  
      });
    }

  ngOnInit(): void {
    this.route.url.subscribe(data => {
      this.authType = data[data.length - 1].path;
     
    });
   
  }

  onPageRegister() {
    this.rout.navigate(['/registrer']);
  }

  onLogin(user:Utilisateur) {
 
    this.request.username=user.username
    this.request.pwd= user.pwd
 
    this.loginService.authenticate(this.request)
    .subscribe(
      
      data => {
        if(data){
        this.user.id = this.loginService.getUserId().slice(1, -1);  
     this.rout.navigate(['/mylist', this.user.id]);
       }else{
        this.errMsg="L'Email ou mot de passe est incorrect"
       }});
   

 
  
}


  get registreFormControl() {
    return this.registreForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.registreForm.valid) {
    }
  }
  onPageLogin() {
    this.rout.navigate(['/login']);
  }

  onRegister(user:Utilisateur) {
    this.request.username=user.username
    this.request.pwd= user.pwd
    this.loginService.ajouterUnUser(this.request)
    .subscribe(data => { console.log(data)
      let id=this.loginService.getUserId().slice(1, -1);
      this.rout.navigate(['/mylist', id]);

     })
  }


  
}

